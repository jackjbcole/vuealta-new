jQuery(document).ready(function($) {    
  $('.home-hero').mousemove(function(e){
    var wx = $(window).width();
    var wy = $(window).height();

    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;
	
    var newx = x - wx/2;
    var newy = y - wy/2;
		
    $('#layers_effect div').each(function(){
      var speed = $(this).attr('data-speed');
      if($(this).attr('data-revert')) speed *= -1;
      TweenMax.to($(this), 1, {x: (1 - newx*speed), y: (1 - newy*speed)});
    });	
  });
  
  $('.testimonials .logo').click(function() {
    var activelogo = $(this).attr('id');
    
    activelogo = activelogo.replace('logo-', '');
    
    $('.testimonials .testimonial-container').removeClass('active');
    $('#testimonial-'+activelogo).addClass('active');    
    
    $('.testimonials .logo').removeClass('active');
    $('#logo-'+activelogo).addClass('active');
  });
  
  // When the user scrolls the page, execute myFunction 
  window.onscroll = function() {myFunction()};

  // Get the header
  var header = document.getElementById("masthead");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("scroll");
    } else {
      header.classList.remove("scroll");
    }
  }
  
  $('.block-select').click(function(e) {
    e.preventDefault();
    var id = $(this).attr('id');
    var num = id.replace('select-', '');
    
    $('.num-block').removeClass('active');
    $('#top-' + num).addClass('active');    
    $('#bottom-' + num).addClass('active');

  });
  
  function menuToggle(e, click) {
    e.preventDefault();
    e.stopPropagation();
    console.log (click);
    console.log ('Click menu item with child');
    $(".sub-menu-overlay").hide();
    $(click).next('.sub-menu-overlay').toggle();
    $('.site-header').addClass('menu-active');
  }
  
  $('.solutions-menu a:first').click(function(e) {
    menuToggle(e, this);
  });  
  
  $('.services-menu a:first').click(function(e) {
    menuToggle(e, this);
  });  
  $('.about-menu a:first').click(function(e) {
    menuToggle(e, this);
  });
    
  $(document).click(function(){
    $(".sub-menu-overlay").hide();
    $('.site-header').removeClass('menu-active');
  });
});