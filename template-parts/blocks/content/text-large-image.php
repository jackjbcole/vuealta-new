<section class="text-image" style="background-color:<?php the_field ('background_color');?>">
  <div class="container">
    <div class="row  <?php if (get_field('layout') == 'text-right'):?>flex-row-reverse<?php endif;?>">
      <div class="col-sm-6">
        <h3><?php the_field('title');?></h3>
        <?php the_field ('text');?>
        <a href="<?php the_field ('cta_url');?>" class="btn <?php the_field ('button_style');?>">
          <span><?php the_field ('cta_text');?></span>
          <div class="arrows">
            <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
            <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
          </div>
        </a>
      </div>
      <div class="break <?php if (get_field('layout') == 'text-right'):?>break-left<?php endif;?>"">
        <img src="<?php the_field('image');?>">
      </div>
    </div>
  </div>
</section>