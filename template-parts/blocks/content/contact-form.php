<section class="contact-form-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 offset-sm-3">
        <h2 class="darkblue_color">Get in touch</h2>
        <?php echo do_shortcode('[contact-form-7 id="1051" title="Contact form 1"]'); ?>
      </div>
    </div>
  </div>
</section>