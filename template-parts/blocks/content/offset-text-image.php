<section class="offset-text-image">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 offset-image">
        <img src="<?php the_field('image');?>">
      </div>
      <div class="col-sm-7 offset-text">
        <h3><?php the_field('title');?></h3>
        <?php the_field ('text');?>
        <?php if (have_rows('logos')):?>
        <div class="logo-row">
          <?php while(have_rows('logos')):the_row('logos');?>
            <img src="<?php the_sub_field('logo');?>" class="logo-row-image"?>
          <?php endwhile;?>
        </div>
        <?php endif;?>
      </div>
    </div>
  </div>
</section>