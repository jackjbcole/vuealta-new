<section class="cta" style="background-color: <?php the_field ('background_color');?>">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <a href="<?php the_field ('cta_url');?>" class="btn <?php the_field ('button_style');?>">
          <span><?php the_field ('cta_text');?></span>
          <div class="arrows">
            <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
            <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>