<section class="locations">
  <div class="container">
    <div class="row">
      <div class="col col-sm-10 offset-sm-1 locations-box marginbottom40">
        <h2>Our Locations</h2>
        <p>Alternatively, contact us by email, phone or drop into one of our offices.</p>
      </div>
    </div>
    <div class="row">
      <?php if (have_rows('regions')):while(have_rows('regions')):the_row();?>
      <?php $count = count(get_sub_field('offices'));?>
      
      <div class="col region <?php if ($count == 4): echo 'col-12'; endif?>">
        <h2><?php the_sub_field ('region');?></h2>
        <?php if (have_rows('offices')):?>
        <div class="row">
          <?php while(have_rows('offices')):the_row();?>
            <div class="col">
              <h3><?php the_sub_field ('city');?></h3>
              <p><?php the_sub_field ('address');?></p>
              <?php if (get_sub_field('email')):?>
              <p><strong>E:</strong> <a href="mailto:<?php the_sub_field('email');?>"><?php the_sub_field('email');?></a></p>
              <?php endif;?>           
              <?php if (get_sub_field('telephone')):?>
              <p><strong>T:</strong> <a href="tel:<?php the_sub_field('telephone');?>"><?php the_sub_field('telephone');?></a></p>
              <?php endif;?>
            </div>
          <?php endwhile;?>
        </div>
        <?php endif;?>
      </div>
      <?php endwhile; endif;?>
    </div>
  </div>
</section>