<section class="home-title">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <h2 class="darkblue_color"><?php the_field ('title');?></h2>
      </div>
    </div>
  </div>
</section>