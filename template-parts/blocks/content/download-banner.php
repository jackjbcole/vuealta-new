<section class="download-banner" style="background-image:linear-gradient(black, black),url('<?php the_field('background_image');?>)">
  <div class="container">
    <div class="row align-items-center">
      <div class="col col-sm-8">
        <h2 class="h2-yellow no-border">
          <?php the_field ('title');?>
        </h2>
        <div class="download-text">
          <?php the_field ('text');?>
        </div>
      </div>
      <div class="col col-sm-4">
        <a href="<?php the_field('cta_url');?>" class="btn <?php the_field ('button_style');?>">
          <span><?php the_field('cta_text');?></span>
        </a>
      </div>
    </div>  
  </div>
</section>