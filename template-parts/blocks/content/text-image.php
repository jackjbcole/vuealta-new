<section class="text-image" style="background-color:<?php the_field ('background_color');?>">
  <div class="container">
    <div class="row <?php if (get_field('layout') == 'text-right'):?>flex-row-reverse<?php endif;?>">
        <div class="col-sm-7">
          <h3><?php the_field('title');?></h3>
          <?php the_field ('text');?>
          <?php if (have_rows('logos')):?>
          <div class="logo-row">
            <?php while(have_rows('logos')):the_row('logos');?>
              <img src="<?php the_sub_field('logo');?>" class="logo-row-image"?>
            <?php endwhile;?>
          </div>
          <?php endif;?>
        </div>
        <div class="col-sm-5">
          <img src="<?php the_field('image');?>">
        </div>
    </div>
  </div>
</section>