<section class="customers">
  <div class="container">
    <?php 
      $testimonials_args = array (
        'post_type' => 'testimonials',
        'orderby'   => 'ID',
        'order'     => 'ASC'
      );

      $count = 0;
      $logocount = 0;

      $testimonials_query = new WP_Query( $testimonials_args );

      if ($testimonials_query->have_posts()):
    ?>

        <?php 
        while($testimonials_query->have_posts()):$testimonials_query->the_post();
        ?>
        <div class="row">
        <div class="col col-sm-8 offset-sm-2">
          <div id="logo-<?php echo $logocount;?>" class="col col-sm-1 logo">
            <img src="<?php the_field ('company_logo', get_the_id());?>"/>
          </div>
          <div>
            <div class="testimonial">
              <?php the_field('testimonial', get_the_id());?>
            </div>
            <div class="referee">
              <?php the_field('referee_title', get_the_id());?>
            </div>
          </div>
        </div>
          </div>
        <?php endwhile;?>

    <?php endif;?>
  </div>
</section>