<section class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col col-12 text-center">
        <h2 class="darkblue_color"><?php the_field('title');?></h2>
      </div>
    </div>
    <?php 
      $testimonials_args = array (
        'post_type' => 'testimonials',
        'orderby'   => 'ID',
        'order'     => 'ASC'
      );

      $count = 0;
      $logocount = 0;

      $testimonials_query = new WP_Query( $testimonials_args );

      if ($testimonials_query->have_posts()):
    ?>
    <div class="row">
      <div class="col col-sm-8 offset-sm-2">
        <?php 
        while($testimonials_query->have_posts()):$testimonials_query->the_post();

        $count++; 
        ?>
        <div id="testimonial-<?php echo $count;?>" class="testimonial-container text-center <?php if ($count == 1) { echo 'active';}?>">
          <div class="testimonial">
            <?php the_field('testimonial', get_the_id());?>
          </div>
          <div class="referee">
            <?php the_field('referee_title', get_the_id());?>
          </div>
        </div>
        <?php endwhile;?>
      </div>
    </div>
    <div class="row">
      <?php 
        while($testimonials_query->have_posts()):$testimonials_query->the_post();

        $logocount++;
      ?>
      <div id="logo-<?php echo $logocount;?>" class="col col-sm-1 logo <?php if ($logocount == 1) { echo 'active';}?>">
        <img src="<?php the_field ('company_logo', get_the_id());?>"/>
      </div>
      <?php endwhile; ?>
    </div>
    <?php endif;?>
  </div>
</section>