<section class="text-block">
  <div class="container">
    <div class="row">
      <div class="col col-sm-10 offset-1">
        <?php the_field('text');?>
      </div>
    </div>
  </div>
</section>