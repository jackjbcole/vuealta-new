<?php if(have_rows('numbered_blocks')):?>
  <section class="numbered-blocks">
    <?php $count = 0;?>
    <?php while(have_rows('numbered_blocks')):the_row();?>
      <div class="num-block <?php if ($count == 0): echo 'active'; endif;?>" id="top-<?php echo $count;?>">
        <div class="container">
          <div class="row">
            <div class="col col-sm-6">
              <h2><?php the_sub_field ('title');?></h2>
            </div>
            <div class="col col-sm-6">
              Image goes here
            </div>
          </div>
        </div>  
      </div>  
    <?php $count++;?>
    <?php endwhile;?>
  </section>
  <section class="pale_grey">
    <?php $count = 0;?>
    <?php while(have_rows('numbered_blocks')):the_row();?>
    <div class="num-block <?php if ($count == 0): echo 'active'; endif;?>" id="bottom-<?php echo $count;?>">
      <div class="container">
        <div class="row">
          <div class="col col-sm-5">
            <h2>Key Features</h2>
            <?php if (have_rows('key_features_list')):?>
            <ul class="key-features-list">
              <?php while(have_rows('key_features_list')):the_row();?>
              <li><?php the_sub_field('feature');?></li>
              <?php endwhile;?>
            </ul>
            <?php endif;?>
          </div>
          <div class="col col-sm-7">
            <h2>Typical Customer Results</h2>
            <?php if (have_rows('results_list')):?>
            <ul class="results-list">
              <?php while(have_rows('results_list')):the_row();?>
              <li><?php the_sub_field('result');?></li>
              <?php endwhile;?>
            </ul>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>  
    <?php $count++;?>
    <?php endwhile;?>
  </section>
<?php endif;?>