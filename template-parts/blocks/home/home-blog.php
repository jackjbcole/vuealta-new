<section class="home-blog">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <h2 class="darkblue_color"><?php the_field ('title');?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col col-sm-6">
        <?php
        $post_args = array (
          'post_type' => 'post',
          'orderby'   => 'date',
          'order'     => 'DESC',
          'posts_per_page' => 1,
        );

        $post_query = new WP_Query( $post_args );

        if ($post_query->have_posts()):while($post_query->have_posts()):$post_query->the_post();
        ?>
        <div class="row">
          <div class="col col-sm-6">
            <a href="<?php the_permalink();?>" class="post-image" style="background-image:url(<?php the_post_thumbnail_url();?>)">     
            </a>
          </div>
          <div class="col col-sm-6">
            <div class="date">
              <?php the_date();?>
            </div>
            <h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
            <div class="excerpt">
              <?php the_excerpt();?>
            </div>
            <a href="<?php the_permalink();?>" class="read-more">
              Read More
            </a>
          </div>
        </div>
        <?php endwhile;endif;?>
      </div>
      <div class="col col-sm-6">
        <?php
        $post_args = array (
          'post_type'       => 'post',
          'orderby'         => 'date',
          'order'           => 'DESC',
          'posts_per_page'  => 2,
          'offset'          => 1,
        );

        $post_query = new WP_Query( $post_args );

        if ($post_query->have_posts()):while($post_query->have_posts()):$post_query->the_post();
        ?>
        <div class="row">
          <div class="col col-sm-3">

          </div>
          <div class="col col-sm-9">
            <div class="date">
              <?php the_date();?>
            </div>
            <h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
          </div>
        </div>
        <?php endwhile; endif;?>
      </div>
    </div>
  </div>
</section>