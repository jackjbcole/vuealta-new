<section class="home-about">
  <div class="container">
    <div class="row">
      <div class="col col-sm-10 offset-sm-1">
        <div class="row">
          <div class="col col-12 col-sm-7">
            <div class="about-title">
              <?php the_field('about_title');?>
            </div>
          </div>
          <div class="col col-sm-5 text-center">
            <img src="<?php the_field('about_logo');?>">
          </div>
        </div>
        <div class="row">
          <div class="col col-12">
            <div class="about-text">
              <?php the_field ('about_text');?>
            </div>
          </div>
          <div class="col col-12">
            <div class="row">
            <?php 
            $testimonials_args = array (
              'post_type' => 'testimonials',
              'orderby'   => 'ID',
              'order'     => 'ASC'
            );

            $testimonials_query = new WP_Query( $testimonials_args );

            if ($testimonials_query->have_posts()):while($testimonials_query->have_posts()):$testimonials_query->the_post();
            ?>
              <div class="col logo">
                <img src="<?php the_field ('company_logo', get_the_id());?>"/>
              </div>
            <?php endwhile; endif;?>
            </div>
          </div>
          <div class="col col-12 col-sm-4">
            <a href="<?php the_field('about_cta_url');?>" class="btn btn-border fullwidth_mob">
              <span><?php the_field ('about_cta_text');?></span>
              <div class="arrows">
                <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
                <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>