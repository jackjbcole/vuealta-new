<section class="home-solutions">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <h2 class="h2-white marginbottom110"><?php the_field ('solutions_title');?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col col-12 col-sm-6">
        <img src="<?php the_field('solutions_logo');?>">
        <div class="solutions-text">
          <?php the_field('solutions_text');?>
        </div>
        <a href="<?php the_field('solutions_cta_url');?>"class="btn btn-border font-white fullwidth_mob">
          <span><?php the_field ('solutions_cta_text');?> </span>
          <div class="arrows">
            <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
            <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
          </div>
        </a>
      </div>
      <div class="col col-12 col-sm-6">
        <div class="row">
          <?php 
            $solutions_args = array (
              'post_type' => 'solutions',
              'orderby'   => 'ID',
              'order'     => 'ASC'
            );
            $solutions_query = new WP_Query( $solutions_args );
          
          if ($solutions_query->have_posts()):while($solutions_query->have_posts()):$solutions_query->the_post();
          ?>
          <div class="col col-6 text-center solutions-container">
            <a href="<?php the_permalink();?>" class="link_icon">
              <div class="hover_triangle">
                <svg xmlns="http://www.w3.org/2000/svg" width="85" height="91" viewBox="0 0 85 91"><path d="M1207.5,1975l-42.5-91h85Z" transform="translate(-1165 -1884)"/></svg>
              </div>
              <div class="feat_icon">
                <img src="<?php the_post_thumbnail_url();?>" alt="Anaplan for <?php the_title();?>">
              </div>
              <div class="feat_title">
                <?php if (get_the_title() == 'Applications'){
                  echo 'Pre-Configured';
                } else {
                  echo 'Anaplan for';
                } ?>
                <span><?php the_title();?></span>
              </div>  
            </a>  
          </div>
          <?php endwhile; endif?>
        </div>
      </div>
    </div>
  </div>
</section>