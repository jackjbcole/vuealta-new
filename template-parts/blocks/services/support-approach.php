<section class="support-approach">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 class="nubbin">
          <?php the_field ('title');?></h2>
      </div>
    </div>
<?php if (have_rows('support_steps')):?>
  <?php $count = 0;?>
  <?php while(have_rows('support_steps')):the_row();?>
    <div class="row">
      <div class="step col col-sm-8 <?php if ($count % 2 == 0) { echo 'offset-sm-4';}?> bg-num bg-num-<?php echo $count;?>">
        <div class="row">
          <div class="col-sm-6">
            <img src="<?php the_sub_field('image');?>">
          </div>
          <div class="col-sm-6 ">
            <h3><?php the_sub_field('title');?></h3>
            <p><?php the_sub_field('subtitle');?></p>
            <?php if (have_rows('features')):?>
            <ul class="line-list">
              <?php while(have_rows('features')):the_row();?>
              <li><?php the_sub_field('feature');?></li>
              <?php endwhile;?>
            </ul>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  <?php $count++;?>
  <?php endwhile;?>
<?php endif;?>
  </div>
</section>