<section class="solutions-section">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <h2 class="white_color marginbottom40">Our Services</h2>
      </div>
    </div>
    <div class="row">
    <?php 
      $solutions_args = array (
        'post_type' => 'services',
        'orderby'   => 'ID',
        'order'     => 'ASC'
      );
      $solutions_query = new WP_Query( $solutions_args );

    if ($solutions_query->have_posts()):while($solutions_query->have_posts()):$solutions_query->the_post();
    ?>
      <div class="col col-sm-10 offset-sm-1 solutions-container">
        <div class="row">
          <div class="col col-sm-2 text-center">
            <div class="feat_icon">
              <img src="<?php the_post_thumbnail_url();?>" alt="<?php the_title();?>">
            </div>
          </div>
          <div class="col col-sm-10">
            <div class="solutions-title">
              <?php the_title();?>
            </div>
            <p class="solutions-p"><?php the_excerpt();?></p>
            <a href="<?php the_permalink();?>" class="solutions-link">
              Read More
            </a>
          </div>  
        </div>
      </div>
    <?php endwhile; endif?>
    </div>
  </div>
</section>