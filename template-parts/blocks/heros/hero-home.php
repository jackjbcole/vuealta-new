<section class="hero home-hero">
  <div class="container">
    <div class="hero-content">
      <div class="hero-content-inner">
        <h1 class="hero-title darkblue_color">
          <?php the_field('hero_title');?>
        </h1>
        <p>
          <?php the_field('hero_subtitle');?>
        </p>
        <div class="row">
          <div class="col col-12 group_buttons">
            <a href="<?php the_field('yellow_button_url');?>" class="btn btn-border fullwidth_mob">
              <span><?php the_field('yellow_button_text');?></span>
              <div class="arrows">
                <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
                <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
              </div>
            </a>
            <a href="<?php the_field('transparent_button_url');?>" class="btn btn-border border-grey fullwidth_mob">
              <span><?php the_field('transparent_button_text');?></span>
              <div class="arrows">
                <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
                <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
              </div>
            </a>
          </div>      
          <div class="labels_awards">
            <img src="/wp-content/themes/vuealta/assets/img/banner-gold.png" alt="Anaplan Parner Gold">
            <img src="/wp-content/themes/vuealta/assets/img/banner-channel.png" alt="Anaplan Channel Partner of the Year 2019">
          </div>
        </div>
      </div>
    </div>
    <div class="layers_wrapper">
      <div class="system_layers" id="layers_effect">
        <div class="layer bottom_layer" data-speed="0.02"><img src="/wp-content/themes/vuealta/assets/img/bottomlayer.png"></div>
        <div class="layer middle_layer" data-speed="0.01"><img src="/wp-content/themes/vuealta/assets/img/gridlayer.png"></div>
        <div class="layer top_layer" data-speed="0.02" data-revert="true"><img src="/wp-content/themes/vuealta/assets/img/toplayer.png"></div>
      </div>
      <div class="hero_home_triangle">
        <div class="bg_dark_shape"></div>
        <img src="/wp-content/themes/vuealta/assets/img/svg/shape-triangle-hero.svg">
      </div>
    </div>  
  </div>
</section>