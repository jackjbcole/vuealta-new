<section class="hero-standard <?php the_field('background_color');?>">
  <div class="container">
    <div class="row">
      <div class="col col-sm-10 offset-sm-1 text-center">
        <h1 class="white_color"><?php the_field('title');?></h1>
        <p class="subtitle"><?php the_field('subtitle');?></p>
      </div>
    </div>
  </div>
</section>