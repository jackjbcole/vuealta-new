<section class="solution-hero" 
  <?php if (get_field('background_image')):?> style="background-image: url('<?php the_field('background_image');?>')"<?php endif;?>
 >
  <div class="container">
    <div class="row">
      <div class="col col-sm-6">
        <div class="above-title">Solutions</div>
        <h1 class="solution-title"><?php the_field('title');?></h1>
        <p class="subtitle"><?php the_field('subtitle');?></p>
        
        <?php if (have_rows('feature_tick_list')):?>
        <ul class="feature-tick-list">
          <?php while(have_rows('feature_tick_list')):the_row();?>
          <li><?php the_sub_field('feature');?></li>
          <?php endwhile;?>
        </ul>
        <?php endif;?>
        <hr class="dark_grey">
        <?php if(have_rows('numbered_blocks')):?>
        <ol class="num-block-list">
          <?php $count = 0;?>
          <?php while(have_rows('numbered_blocks')):the_row();?>
          <li>
            <a href="" class="block-select" id="select-<?php echo $count ?>"><?php the_sub_field ('title');?></a>
          </li>
          <?php $count++;?>
          <?php endwhile;?>

        </ol>
        <?php endif;?>
      </div>
    </div>
  </div>
</section>