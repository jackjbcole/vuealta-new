<section class="hero-solutions">
  <div class="container">
    <div class="row">
      <div class="col col-sm-12 text-center">
        <h1 class="white_color marginbottom40"><?php the_field('title');?></h1>
        <div class="hero-subtitle">
          <?php the_field('subtitle');?>
        </div>
      </div>
    </div>
    <div class="row">
    <?php 
      $solutions_args = array (
        'post_type' => 'solutions',
        'orderby'   => 'ID',
        'order'     => 'ASC'
      );
      $solutions_query = new WP_Query( $solutions_args );

    if ($solutions_query->have_posts()):while($solutions_query->have_posts()):$solutions_query->the_post();
    ?>
      <div class="col text-center solutions-container">
         <div class="feat_icon">
            <img src="<?php the_post_thumbnail_url();?>" alt="Anaplan for Sales">
          </div>
          <div class="feat_title">
            <?php if (get_the_title() == 'Applications'){
              echo 'Pre-Configured';
            } else {
              echo 'Anaplan for';
            } ?>
             <span><?php the_title();?></span>
          </div>  
      </div>
    <?php endwhile; endif?>
    </div>
  </div>
</section>