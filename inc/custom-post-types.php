<?php 


function custom_post_types() {
	$solutionslabels = array(
		'name'               => _x( 'Solutions', 'post type general name', 'vuealta' ),
		'singular_name'      => _x( 'Solution', 'post type singular name', 'vuealta' ),
		'menu_name'          => _x( 'Solutions', 'admin menu', 'vuealta' ),
		'name_admin_bar'     => _x( 'Solution', 'add new on admin bar', 'vuealta' ),
		'add_new'            => _x( 'Add New', 'solution', 'vuealta' ),
		'add_new_item'       => __( 'Add New Solution', 'vuealta' ),
		'new_item'           => __( 'New Solution', 'vuealta' ),
		'edit_item'          => __( 'Edit Solution', 'vuealta' ),
		'view_item'          => __( 'View Solution', 'vuealta' ),
		'all_items'          => __( 'All Solutions', 'vuealta' ),
		'search_items'       => __( 'Search Solutions', 'vuealta' ),
		'parent_item_colon'  => __( 'Parent Solutions:', 'vuealta' ),
		'not_found'          => __( 'No services found.', 'vuealta' ),
		'not_found_in_trash' => __( 'No services found in Trash.', 'vuealta' )
	);

	$solutionsargs = array(
		'labels'             => $solutionslabels,
		'description'        => __( 'Description.', 'vuealta' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
  
  	register_post_type( 'solutions', $solutionsargs );	
  
    $serviceslabels = array(
		'name'               => _x( 'Services', 'post type general name', 'vuealta' ),
		'singular_name'      => _x( 'Service', 'post type singular name', 'vuealta' ),
		'menu_name'          => _x( 'Services', 'admin menu', 'vuealta' ),
		'name_admin_bar'     => _x( 'Service', 'add new on admin bar', 'vuealta' ),
		'add_new'            => _x( 'Add New', 'service', 'vuealta' ),
		'add_new_item'       => __( 'Add New Service', 'vuealta' ),
		'new_item'           => __( 'New Service', 'vuealta' ),
		'edit_item'          => __( 'Edit Service', 'vuealta' ),
		'view_item'          => __( 'View Service', 'vuealta' ),
		'all_items'          => __( 'All Services', 'vuealta' ),
		'search_items'       => __( 'Search Services', 'vuealta' ),
		'parent_item_colon'  => __( 'Parent Services:', 'vuealta' ),
		'not_found'          => __( 'No services found.', 'vuealta' ),
		'not_found_in_trash' => __( 'No services found in Trash.', 'vuealta' )
	);

	$servicesargs = array(
		'labels'             => $serviceslabels,
		'description'        => __( 'Description.', 'vuealta' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
  
  	register_post_type( 'services', $servicesargs ); 
  
    $testimoniallabels = array(
		'name'               => _x( 'Testimonials', 'post type general name', 'vuealta' ),
		'singular_name'      => _x( 'Testimonial', 'post type singular name', 'vuealta' ),
		'menu_name'          => _x( 'Testimonials', 'admin menu', 'vuealta' ),
		'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'vuealta' ),
		'add_new'            => _x( 'Add New', 'testimonial', 'vuealta' ),
		'add_new_item'       => __( 'Add New Testimonial', 'vuealta' ),
		'new_item'           => __( 'New Testimonial', 'vuealta' ),
		'edit_item'          => __( 'Edit Testimonial', 'vuealta' ),
		'view_item'          => __( 'View Testimonial', 'vuealta' ),
		'all_items'          => __( 'All Testimonials', 'vuealta' ),
		'search_items'       => __( 'Search Testimonials', 'vuealta' ),
		'parent_item_colon'  => __( 'Parent Testimonials:', 'vuealta' ),
		'not_found'          => __( 'No testimonials found.', 'vuealta' ),
		'not_found_in_trash' => __( 'No testimonials found in Trash.', 'vuealta' )
	);

	$testimonialargs = array(
		'labels'             => $testimoniallabels,
		'description'        => __( 'Description.', 'vuealta' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
  
  	register_post_type( 'testimonials', $testimonialargs );
  
  	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'vuealta' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'vuealta' ),
		'menu_name'             => __( 'Events', 'vuealta' ),
		'name_admin_bar'        => __( 'Event', 'vuealta' ),
		'archives'              => __( 'Event Archives', 'vuealta' ),
		'attributes'            => __( 'Event Attributes', 'vuealta' ),
		'parent_item_colon'     => __( 'Parent Item:', 'vuealta' ),
		'all_items'             => __( 'All Items', 'vuealta' ),
		'add_new_item'          => __( 'Add New Item', 'vuealta' ),
		'add_new'               => __( 'Add New', 'vuealta' ),
		'new_item'              => __( 'New Item', 'vuealta' ),
		'edit_item'             => __( 'Edit Item', 'vuealta' ),
		'update_item'           => __( 'Update Item', 'vuealta' ),
		'view_item'             => __( 'View Item', 'vuealta' ),
		'view_items'            => __( 'View Items', 'vuealta' ),
		'search_items'          => __( 'Search Item', 'vuealta' ),
		'not_found'             => __( 'Not found', 'vuealta' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vuealta' ),
		'featured_image'        => __( 'Featured Image', 'vuealta' ),
		'set_featured_image'    => __( 'Set featured image', 'vuealta' ),
		'remove_featured_image' => __( 'Remove featured image', 'vuealta' ),
		'use_featured_image'    => __( 'Use as featured image', 'vuealta' ),
		'insert_into_item'      => __( 'Insert into item', 'vuealta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'vuealta' ),
		'items_list'            => __( 'Items list', 'vuealta' ),
		'items_list_navigation' => __( 'Items list navigation', 'vuealta' ),
		'filter_items_list'     => __( 'Filter items list', 'vuealta' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'vuealta' ),
		'description'           => __( 'Events custom post type', 'vuealta' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'				=> 'dashicons-tickets-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'rewrite'				=> array( 
			'slug' => 'events',
			'with_front' => false,
		), 
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'events', $args );
	
	
	$career_labels = array(
		'name'                  => _x( 'Careers', 'Post Type General Name', 'vuealta' ),
		'singular_name'         => _x( 'Career', 'Post Type Singular Name', 'vuealta' ),
		'menu_name'             => __( 'Careers', 'vuealta' ),
		'name_admin_bar'        => __( 'Career', 'vuealta' ),
		'archives'              => __( 'Career Archives', 'vuealta' ),
		'attributes'            => __( 'Career Attributes', 'vuealta' ),
		'parent_item_colon'     => __( 'Parent Item:', 'vuealta' ),
		'all_items'             => __( 'All Items', 'vuealta' ),
		'add_new_item'          => __( 'Add New Item', 'vuealta' ),
		'add_new'               => __( 'Add New', 'vuealta' ),
		'new_item'              => __( 'New Item', 'vuealta' ),
		'edit_item'             => __( 'Edit Item', 'vuealta' ),
		'update_item'           => __( 'Update Item', 'vuealta' ),
		'view_item'             => __( 'View Item', 'vuealta' ),
		'view_items'            => __( 'View Items', 'vuealta' ),
		'search_items'          => __( 'Search Item', 'vuealta' ),
		'not_found'             => __( 'Not found', 'vuealta' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vuealta' ),
		'featured_image'        => __( 'Featured Image', 'vuealta' ),
		'set_featured_image'    => __( 'Set featured image', 'vuealta' ),
		'remove_featured_image' => __( 'Remove featured image', 'vuealta' ),
		'use_featured_image'    => __( 'Use as featured image', 'vuealta' ),
		'insert_into_item'      => __( 'Insert into item', 'vuealta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'vuealta' ),
		'items_list'            => __( 'Items list', 'vuealta' ),
		'items_list_navigation' => __( 'Items list navigation', 'vuealta' ),
		'filter_items_list'     => __( 'Filter items list', 'vuealta' ),
	);
	$career_args = array(
		'label'                 => __( 'Careers', 'vuealta' ),
		'description'           => __( 'Careers custom post type', 'vuealta' ),
		'labels'                => $career_labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'				=> 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,	   	
		'rewrite'				=> array( 
			'slug' => 'careers',
			'with_front' => false,
		), 
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'careers', $career_args );
}

add_action( 'init', 'custom_post_types', 0 );