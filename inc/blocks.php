<?php

function jbc_blocks ( $categories, $post ) {
  return array_merge(
    $categories,
    array(
      array(
        'slug' => 'hero-blocks',
        'title' => __( 'Hero Blocks', 'vuealta' ),
      ),
      array(
        'slug' => 'home-blocks',
        'title' => __( 'Home Blocks', 'vuealta' ),
      ) ,   
      array(
        'slug' => 'solution-blocks',
        'title' => __( 'Solution Blocks', 'vuealta' ),
      ),
      array(
        'slug' => 'content-blocks',
        'title' => __( 'Content Blocks', 'vuealta')
      )
    )
  );
}
add_filter( 'block_categories', 'jbc_blocks', 10, 2);

function register_acf_block_types() {
    
  acf_register_block_type(array(
    'name'              => 'hero-home',
    'title'             => __('Homepage Hero Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/heros/hero-home.php',
    'category'          => 'hero-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));      
  
  acf_register_block_type(array(
    'name'              => 'hero-solutions',
    'title'             => __('Solutions Hero Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/heros/hero-solutions.php',
    'category'          => 'hero-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'home-about',
    'title'             => __('Homepage About Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/home/home-about.php',
    'category'          => 'home-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));
  
  acf_register_block_type(array(
    'name'              => 'home-solutions',
    'title'             => __('Homepage Solutions Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/home/home-solutions.php',
    'category'          => 'home-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',           
  )); 
  
  acf_register_block_type(array(
    'name'              => 'title',
    'title'             => __('Title Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/title.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',           
  )); 
  
  acf_register_block_type(array(
    'name'              => 'offset-text-image',
    'title'             => __('Offset Text and Image Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/offset-text-image.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',           
  ));  
  
  acf_register_block_type(array(
    'name'              => 'text-image',
    'title'             => __('Text and Image Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/text-image.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',           
  ));  
  
  acf_register_block_type(array(
    'name'              => 'call-to-action',
    'title'             => __('Call to Action Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/cta.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',           
  ));
  
  acf_register_block_type(array(
    'name'              => 'testimonials',
    'title'             => __('Testimonial Section'),
      'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/testimonials.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'download-banner',
    'title'             => __('Download Banner Section'),
      'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/download-banner.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'home-blog',
    'title'             => __('Home Blog Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/home/home-blog.php',
    'category'          => 'home-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));
  
  acf_register_block_type(array(
    'name'              => 'text',
    'title'             => __('Text Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/text.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'text-large-image',
    'title'             => __('Text and Large Image Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/content/text-large-image.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
 
  acf_register_block_type(array(
    'name'              => 'solutions',
    'title'             => __('Solutions Section'),
    'description'       => __('A custom testimonial block.'),
    'render_template'   => 'template-parts/blocks/solutions/solutions.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'hero-standard',
    'title'             => __('Hero Standard'),
    'description'       => __('A standard text hero section.'),
    'render_template'   => 'template-parts/blocks/heros/hero-standard.php',
    'category'          => 'hero-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'contact-form',
    'title'             => __('Contact Form'),
    'description'       => __('Contact form section.'),
    'render_template'   => 'template-parts/blocks/content/contact-form.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'locations',
    'title'             => __('Locations'),
    'description'       => __('Our Locations section.'),
    'render_template'   => 'template-parts/blocks/content/locations.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'services',
    'title'             => __('Services Section'),
    'description'       => __('Our Services section.'),
    'render_template'   => 'template-parts/blocks/services/services.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'support-apporach',
    'title'             => __('Support Approach Section'),
    'description'       => __('Support Approach  section.'),
    'render_template'   => 'template-parts/blocks/services/support-approach.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));  
  
  acf_register_block_type(array(
    'name'              => 'customer-list',
    'title'             => __('Customer List Section'),
    'description'       => __('List of all customers.'),
    'render_template'   => 'template-parts/blocks/content/customers.php',
    'category'          => 'content-blocks',
    'icon'              => 'admin-comments',
    'mode'              => 'edit',
  ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
  add_action('acf/init', 'register_acf_block_types');
}
