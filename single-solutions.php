<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vuealta
 */

get_header();
?>

<?php get_template_part ('template-parts/blocks/heros/hero-solution');?>
<?php get_template_part ('template-parts/blocks/solutions/content');?>

<?php get_footer();
