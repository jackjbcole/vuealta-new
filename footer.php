<?php
/**
 *
 * Footer
 *
 **/
?>
  </div>
  <footer id="colophon" class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col col-12 col-sm-6">
          <h2 class="no-border">Ready to <span>Get Started?</span></h2>
          
          <a href="/request-demo" class="btn btn-border font-white fullwidth_mob marginbottom40">
          <span>Request a Demo </span>
          <div class="arrows">
            <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
            <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
          </div>
        </a>
		</div>
        <div class="col col-12 col-sm-3">
          <h3>Solutions</h3>
          <nav class="solutions-menu">
              <?php
              wp_nav_menu( array(
                  'theme_location' => 'solutions',
                  'menu_id'        => 'solutions-menu',
                  'menu_class'     => 'solutions-nav',
              ) );
              ?>
          </nav><!-- #site-navigation -->
        </div>      
        <div class="col col-21 col-sm-3">
          <h3>Services</h3>
              <?php
              wp_nav_menu( array(
                  'theme_location' => 'services',
                  'menu_id'        => 'sservices-menu',
                  'menu_class'     => 'solutions-nav',
              ) );
              ?>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 col-sm-4 social">
          Follow us on social menu
        </div>
        <div class="col col-12 col-sm-4 offset-sm-4 terms text-right">
            &copy; <?php echo date('Y'); ?> Vuealta
          
          Boring stuff menu
        </div>
      </div>
    </div>
  </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
