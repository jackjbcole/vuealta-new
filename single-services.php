<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vuealta
 */

get_header();
?>

<section>
  <div class="container">
    <div class="row">
      <div class="col">
        <h1><?php the_title();?></h1>
      </div>
    </div>
  </div>
</section>

<?php get_footer();
