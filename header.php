<?php
/**
 *
 * Header
 *
 **/

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
  <header id="masthead" class="site-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col col-sm-2 site-branding">
          <h1 class="site-title">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo-link">
              <img src="/wp-content/themes/vuealta/assets/img/vuealta-logo-white-bg.png"/>
            </a>
          </h1>
        </div><!-- .site-branding -->
        <div class="col offset-sm-1  navigation">
          <nav id="site-navigation" class="main-navigation">
              <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'vuealta' ); ?></button>
              <?php
              wp_nav_menu( array(
                  'theme_location' => 'menu-1',
                  'menu_id'        => 'primary-menu',
                  'menu_class'     => 'main-nav',
                  'walker'         => new WPSE_78121_Sublevel_Walker
              ) );
              ?>
          </nav><!-- #site-navigation -->
        </div>
        <!--div class="col lang">
          <nav id="lang-nav">
            <ul>
              <li>Eng</li>
            </ul>
          </nav> 
        </div-->
        <div class="col demo-container text-right">
          <a class="btn btn-border btn-solid btn-small" href="/request-demo">
            <span>Request a Demo</span>
            <div class="arrows">
              <div class="arrow default"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
              <div class="arrow hover"><svg viewBox="0 0 4 7"><path d="M.7 0L0 .7l2.7 2.8L0 6.3l.7.7L4 3.5z"></path></svg></div>
            </div>
          </a>
        </div>
      </div>
    </div>

  </header><!-- #masthead -->
  <div class="content">